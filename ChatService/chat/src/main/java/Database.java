import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by michel on 10-6-2016.
 */
public class Database{
    private String dbname;
    public Database() throws Exception{
        dbname = "chatservice.db";

        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("INIT: Database successfully opened");

            Statement stmt = c.createStatement();
            String user = "CREATE TABLE IF NOT EXISTS User " +
                            "(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                            "Name TEXT NOT NULL," +
                            "Password TEXT NOT NULL);";
            String chat = "CREATE TABLE IF NOT EXISTS Chat " +
                            "(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                            "SID INT NOT NULL," +
                            "RID INT NOT NULL," +
                            "Time_Started TEXT NOT NULL);";
            String chatregel = "CREATE TABLE IF NOT EXISTS Chatregel " +
                                "(ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"+
                                "ChatID INT NOT NULL," +
                                "UID INT NOT NULL," +
                                "Message TEXT NOT NULL," +
                                "Time TEXT NOT NULL);";
            stmt.execute(user);
            stmt.execute(chat);
            stmt.execute(chatregel);
            stmt.close();
            c.close();
            System.out.println("INIT: Table User successfully made");
            System.out.println("INIT: Table Chat successfully made");
            System.out.println("INIT: Table Chatregel succesfully made");
        } catch(Exception e){
            System.out.println("INIT: Exception: " + e.getMessage());
            throw e;
        }
    }

    public Long insertUser(String name, String password) throws Exception {
        Long userId = null;
        try {
                Class.forName("org.sqlite.JDBC");
                Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
                c.setAutoCommit(false);
                System.out.println("InsertUser: Database successfully opened");

                Statement stmt = c.createStatement();
                String sql = "INSERT INTO User(Name,Password) VALUES('" + name + "','" + password + "');";
                stmt.executeUpdate(sql);

                try(ResultSet generatedKeys = stmt.getGeneratedKeys()){
                    if(generatedKeys.next()){
                        System.out.println("InsertUser: Insert successfull");
                        userId = generatedKeys.getLong(1);
                    } else {
                        System.out.println("InsertUser: Insert not successfull");
                    }
                }

                stmt.close();
                c.commit();
                c.close();

        } catch(Exception e){
            System.out.println("InsertUser: Exception: " + e.getMessage());
            throw e;
        }
        return userId;
    }

    public Map getUser(String name, String password) throws Exception{
        Map<String, String> returnData = new HashMap<>();
        Integer id = null;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetUser: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID FROM User WHERE Name='" + name + "' AND Password='" + password + "' LIMIT 1;";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                id = rs.getInt("ID");
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetUser: Select successfull");
            if(id != null){
                returnData.put("UID", Integer.toString(id));
                returnData.put("Check", "True");
            } else {
                returnData.put("Check", "False");
            }
        } catch(Exception e){
            System.out.println("GetUser: Exception: " + e.getMessage());
            throw e;
        }
        return returnData;
    }

    public String getUsernameById(String id) throws Exception{
        String returnData = null;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetUserById: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID, Name FROM User WHERE ID='" + id + "';";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                returnData = rs.getString("Name");
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetUserById: Select successfull");
        } catch(Exception e){
            System.out.println("GetUser: Exception: " + e.getMessage());
            throw e;
        }
        return returnData;
    }

    public Map insertChat(String sid, String rid) throws Exception{
        Map<String, String> returnData = new HashMap<>();
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            c.setAutoCommit(false);
            System.out.println("InsertChat: Database successfully opened");

            Statement stmt = c.createStatement();
            LocalDateTime datetime = LocalDateTime.now();
            String sql = "INSERT INTO Chat(SID,RID,Time_Started) VALUES(" + sid + "," + rid + ",'" + datetime.toString() + "');";
            stmt.executeUpdate(sql);

            try(ResultSet generatedKeys = stmt.getGeneratedKeys()){
                if(generatedKeys.next()){
                    System.out.println("InsertChat: Insert successfull");
                    returnData.put("Check", "True");
                    returnData.put("CID", Long.toString(generatedKeys.getLong(1)));
                    returnData.put("Time_Started", datetime.toString());
                } else {
                    System.out.println("InsertChat: Insert not successfull");
                    returnData.put("Check", "False");
                }
            }

            stmt.close();
            c.commit();
            c.close();
        } catch(Exception e){
            System.out.println("StartChat: Exception: " + e.getMessage());
            throw e;
        }
        return returnData;
    }

    public Map getChat(String sid, String rid) throws Exception{
        Map<String, String> returnData = new HashMap<>();
        Integer id = null;
        String datetime = null;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetChat: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID, Time_Started FROM Chat WHERE (SID=" + sid + " AND RID=" + rid + ") OR (SID=" + rid + " AND RID=" + sid + ") LIMIT 1;";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                id = rs.getInt("ID");
                datetime = rs.getString("Time_Started");
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetChat: Select successfull");
            if(id != null){
                returnData.put("CID", Integer.toString(id));
                returnData.put("Time_Started", datetime);
                returnData.put("RName", getUsernameById(rid));
                returnData.put("SName", getUsernameById(sid));
                returnData.put("Check", "True");
            } else {
                returnData.put("Check", "False");
            }
        } catch(Exception e){
            System.out.println("GetChat: Exception: " + e.getMessage());
            returnData.put("Check", "False");
            throw e;
        }
        return returnData;
    }

    public Map getChatByID(String cid) throws Exception{
        Map<String, String> returnData = new HashMap<>();
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetChat: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID, SID, RID, Time_Started FROM Chat WHERE ID = " + cid + " LIMIT 1;";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                returnData.put("CID", Integer.toString(rs.getInt("ID")));
                returnData.put("Time_Started", rs.getString("Time_Started"));
                returnData.put("SID", Integer.toString(rs.getInt("SID")));
                returnData.put("SName", getUsernameById(Integer.toString(rs.getInt("SID"))));
                returnData.put("RID", Integer.toString(rs.getInt("RID")));
                returnData.put("RName", getUsernameById(Integer.toString(rs.getInt("RID"))));
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetChat: Select successfull");
        } catch(Exception e){
            System.out.println("GetChat: Exception: " + e.getMessage());
            returnData.put("Check", "False");
            throw e;
        }
        return returnData;
    }

    public Map getChats(String id) throws Exception {
        Map<String, Map> returnData = new HashMap<>();
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetChats: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID, Time_Started, SID, RID FROM Chat WHERE SID=" + id + " OR RID=" + id + ";";
            ResultSet rs = stmt.executeQuery(sql);
            int counter = 1;
            while(rs.next()){
                Map<String, String> row = new HashMap<>();
                row.put("ID", Integer.toString(rs.getInt("ID")));
                row.put("Time_Started", rs.getString("Time_Started"));
                row.put("SID", Integer.toString(rs.getInt("SID")));
                row.put("SName", getUsernameById(Integer.toString(rs.getInt("SID"))));
                row.put("RID", Integer.toString(rs.getInt("RID")));
                row.put("RName", getUsernameById(Integer.toString(rs.getInt("RID"))));
                returnData.put(Integer.toString(counter), row);
                counter++;
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetChats: Select successfull");
        } catch(Exception e){
            System.out.println("GetChats: Exception: " + e.getMessage());
            throw e;
        }
        return returnData;
    }

    public Long insertChatregel(String cid, String uid, String message) throws Exception{
        Long id = null;
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            c.setAutoCommit(false);
            System.out.println("InsertChatRegel: Database successfully opened");

            LocalDateTime datetime = LocalDateTime.now();

            Statement stmt = c.createStatement();
            String sql = "INSERT INTO Chatregel(ChatID, UID, Message, Time) VALUES(" + cid + "," + uid + ", '" + message + "', '" + datetime.toString() + "');";
            stmt.executeUpdate(sql);

            try(ResultSet generatedKeys = stmt.getGeneratedKeys()){
                if(generatedKeys.next()){
                    System.out.println("InsertChatRegel: Insert successfull");
                    id = generatedKeys.getLong(1);
                } else {
                    System.out.println("InsertChatRegel: Insert not successfull");
                }
            }

            stmt.close();
            c.commit();
            c.close();

        } catch(Exception e){
            System.out.println("InsertChatRegel: Exception: " + e.getMessage());
            throw e;
        }
        return id;
    }

    public Map getChatregels(String cid) throws Exception{
        Map<String, Map> returnData = new HashMap<>();
        try {
            Class.forName("org.sqlite.JDBC");
            Connection c = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            System.out.println("GetChatregels: Database successfully opened");

            Statement stmt = c.createStatement();
            String sql = "SELECT ID, ChatID, UID, Message, Time FROM Chatregel WHERE ChatID=" + cid + " ORDER BY Time ASC;";
            ResultSet rs = stmt.executeQuery(sql);
            int counter = 1;
            while(rs.next()){
                Map<String, String> row = new HashMap<>();
                row.put("ID", Integer.toString(rs.getInt("ID")));
                row.put("ChatID", Integer.toString(rs.getInt("ChatID")));
                row.put("UID", Integer.toString(rs.getInt("UID")));
                row.put("Time", rs.getString("Time"));
                row.put("Message", rs.getString("Message"));
                returnData.put(Integer.toString(counter), row);
                counter++;
            }
            rs.close();
            stmt.close();
            c.close();
            System.out.println("GetChatregels: Select successfull");
        } catch(Exception e){
            System.out.println("GetChatregels: Exception: " + e.getMessage());
            throw e;
        }
        return returnData;
    }
}
